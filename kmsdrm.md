# How to get KMS/DRM on Odroid devices

If you are not familiar with [KMS/DRM], think of a kind of direct access to a
framebuffer on esteroids (kind of the deprecated [DirectFB], but with more
functionality). It is useful to have games and emulators running without using
[X11] or [Wayland]. Some libraries like SDL2 support KMS/DRM.

[KMS/DRM]: https://en.wikipedia.org/wiki/Direct_Rendering_Manager
[DirectFB]: https://en.wikipedia.org/wiki/DirectFB
[X11]: https://en.wikipedia.org/wiki/X_Window_System
[Wayland]: https://en.wikipedia.org/wiki/Wayland_(display_server_protocol)

## Before you start

Install these packages that will be required and/or useful at some point.

```bash
$ sudo apt install wajig neovim meson tmux g++ gcc gdb clang clangd git cmake silversearcher-ag make mc evtest htop strace
```

And add the `input` and `video` group permissions to `odroid` user:
```
# Needed to read input events: keyboard, mouse, etc.
# If you run "evtest" and you see the keyboard & mouse devices, you don't need to add this.
$ sudo usermod -a -G input odroid

# And just in case, add user to "video" group.
$ sudo usermod -a -G video odroid
```

## Odroid Go Advanced

![oga](https://lh3.googleusercontent.com/PFJ7KScSc92eKNfCWJoQkIVv2PeheM2Yw5JGDUpxko5JquG9WJycWchaFB2-sMwChvZYkIE2vZ1u7VibRgKxB7g3wJaAW21v_dYFonS0KtQ2gkDDYSaTxsZZIdrnapcOvPYt6TALuTI=-no)

TODO: Complete this guide, in the meantime read: [vice-oga.md](https://gitlab.com/ricardoquesada/odroid-misc/-/blob/master/vice-oga.md)

## Odroid N2

![on2](https://lh3.googleusercontent.com/5oOKNai0U5H2ZaUy0sGX3gqAMJK87oK4v0xEZR2_uoW4YD9Mqh4Hhq1WbhbpJaG14qodpfGyNP802iSgCGHzEI54LdTlbd-8Q86-GsQLtU4ZSZ190T6waUBwO36N0jSxd0YgtxexV9Q=-no)

Install "ubuntu 20.04 - minimal" image from here:
http://ppa.linuxfactory.or.kr/images/raw/arm64/focal/ubuntu-20.04-minimal-odroidn2-20200421.img.xz

In this order do:

1. Add missing "contrib" to apt sources:

The "mali-bifrost-dkms" package is part of "contrib" and needed for the GPU driver.

```bash
$ cd /etc/apt/sources.list.d/
$ sudo vi ppa-linuxfactory-or-kr.list
```

and it should look like this:

    deb http://ppa.linuxfactory.or.kr focal main non-free contrib
    deb-src http://ppa.linuxfactory.or.kr focal main non-free contrib

2. Update && Upgrade to linux 5.4.0

Note: Upgrading to a newer kernel version is fine too. Just make sure to upgrade
both the `linux-image` and the `linux-headers` packages.

```bash
$ sudo apt update  && sudo apt upgrade
$ sudo apt install linux-image-5.4.0-odroid-arm64
$ sudo apt install linux-headers-5.4.0-odroid-all
```

3. Reboot

```bash
$ sudo reboot
```

And verify that the kernel is 5.4.0:

```bash
$ uname -a
Linux n2 5.4.0-odroid-arm64 #1 SMP PREEMPT Ubuntu 5.4.39-202005080054~focal (2020-05-07) aarch64 aarch64 aarch64 GNU/Linux
```

If you don't see kernel 5.4, then something went wrong while updating the kernel.
Try again perhaps (?).

4. Install bifrost drivers

```bash
$ sudo apt install mali-bifrost-wayland-driver
```

It should compile and install the `mali_kbase` kernel module. Do:

```bash
$ lsmod | grep mali_
```

And you should see:

    mali_kbase            585728  0

If you don't see the driver installed do:

```bash
$ sudo modprobe mali_kbase
$ lsmod | grep mali_
```

And you should see the driver by now.

5. Go to "Testing KMS/DRM" section

## Odroid C4

![oc4](https://lh3.googleusercontent.com/fzFqdL_n2-ogUAk9niFmaUxte_aEk4CKweyjMhgAZ_TKaEPZdkhQDuUoy0dQISlHSAoQ66bIw4UVPLYBRyDReRu3to0K5BRG1NSUBtfq457eo6IcytELXWHx0LeGFsZ36QwT8pyKSsc=-no)

Install "ubuntu 20.04 - minimal" image from here:
http://ppa.linuxfactory.or.kr/images/raw/arm64/focal/ubuntu-20.04-minimal-odroidc4-20200421.img.xz

1. Install 5.4.0 headers

Note: The image that we are using comes with kernel 5.4 installed. So, the
5.4 `linux-headers` are needed. If you want to use a newer kernel, don't forget to
install both the `linux-image` and `linux-headers` packages.

```bash
$ sudo apt update  && sudo apt upgrade
$ sudo apt install linux-headers-5.4.0-odroid-all
```

2. Install Mali drivers

```bash
$ sudo apt install mali-bifrost-wayland-driver
```

It should compile and install the `mali_kbase` kernel module. Do:

```bash
$ lsmod | grep mali_
```

And you should see:

    mali_kbase            585728  0

If you don't see the driver installed do:

```bash
$ sudo modprobe mali_kbase
$ lsmod | grep mali_
```

And you should see the driver now.

3. Go to "Testing KMS/DRM" section

## Testing KMS/DRM

![kmscube](https://lh3.googleusercontent.com/do4LgCE14GpiXYiz-lKVH2Qswmqwx1VHQwh2Iy_lCGbbsQQuJldPmEh15LO3uHue3JS2PUXs7_XMjYc-Ax_jLbVeyacBzwuIsUYUXRL-2OHU8iUNKFhjOAmQyDKGQLhOiMHOc2GhvV4=-no)

Once you have kernel 5.4+ and the Mali drivers installed, KMS/DRM should work.
To test it do:

```bash
$ sudo apt install kmscube
$ kmscube
```

And run it from the console (not from an ssh connection, that's the whole point
of KMS/DRM).

## Testing toolchain

Now that you can run the precompiled version of kmscube, try compiling it
yourself to make sure that you have the correct headers / libraries installed.

```bash
$ sudo apt install libdrm-dev libgbm-dev libegl-dev libgles-dev libpng-dev pkg-config
```

Download kmscube, compile it and run it:

```bash
$ git clone https://gitlab.freedesktop.org/mesa/kmscube.git
$ cd kmscube
$ meson build/
$ ninja -C build/
$ cd build
$ ./kmscube
```

If you have reached this point, you are good. It means that you can compile
KMS/DRM with EGL + GLES2. Something needed for SDL2.

## Libraries and emulators

### SDL2

![sdl](https://lh3.googleusercontent.com/ogNmV488RJyIkXk0Oi6JhbA2_eBqzla9MpFhmBlzzlQnQF7GRn0Svi56bvvlMWA7772H_Fid1HchEIX9aPI-k8Fy4BrcXVG9Q4oeyAK8UL4H7W81-2XDnwpJRwXI3HczZBD7olef_nc=-no)

**UPDATE**: precompiled SDL2 with KMS/DRM and Wayland support can be installed
by doing: `sudo apt install libsdl2-dev libsdl2-2.0.0`

1. Install needed dev libraries

```bash
$ sudo apt install mercurial autoconf automake libtool
$ sudo apt install libwayland-dev libxkbcommon-dev wayland-protocols
$ sudo apt install libxext-dev
$ sudo apt install libpulse-dev libaudio-dev libasound2-dev
$ sudo apt install libudev-dev libdbus-1-dev
```

2. Download

```bash
$ hg clone http://hg.libsdl.org/SDL
```

3. Compile

Note:
*  `--enable-video-kmsdrm`: enables the KMS/DRM driver
*  `--disable-video-opengl`: disables OpenGL driver. If enabled SDL will try to
init OpenGL instead of OpenGLES and it will fail.
*  `--disable-kmsdrm-shared`: links against libopenegl / libopengles. Needed to
use the Mali driver. Otherwise, if it tries to load it in runtime, it
won't load the correct GLES/EGL driver.

```bash
$ cd SDL
$ ./configure --enable-video-kmsdrm --disable-video-opengl --disable-kmsdrm-shared
```

In the output, you should see "kmsdrm" in Video drivers. E.g:

    Enabled modules : atomic audio video render events joystick haptic sensor power filesystem threads timers file loadso cpuinfo assembly
    Assembly Math   :
    Audio drivers   : disk dummy oss alsa(dynamic) pulse(dynamic) nas(dynamic)
    Video drivers   : dummy x11(dynamic) kmsdrm opengl_es1 opengl_es2 vulkan wayland(dynamic)
    X11 libraries   : xdbe xshape
    Input drivers   : linuxev linuxkd
    Enable virtual joystick APIs : YES
    Using libsamplerate : NO
    Using libudev       : YES
    Using dbus          : YES
    Using ime           : YES
    Using ibus          : NO
    Using fcitx         : NO

And compile it and install it:

```bash
$ make
$ sudo make install
```

4. Test SDL

SDL2 comes with some tests. You should compile them and run them:

```bash
$ cd tests
$ ./configure
$ make
$ ./testgles2 --vsync --accel --fsaa
```

And you should see a smooth rotating cube. If so, it means that you have SDL
running with hardware-accelerated OpenGLES 2.

BUG: On N2, it currently fails because it cannot init EGL. Workaround [here].

BUG2: Even though Wayland is supported by SDL, when hw-accel is enabled, it
fails to create windows.

[here]: https://forum.odroid.com/viewtopic.php?p=291755&sid=f2ac514cbd84545b94661e3a38cf7586#p291755

### VICE

1. Install dependencies

![vice](https://lh3.googleusercontent.com/Z479KSKtAIopyCsQgVYCulX4MIeLOlmP3wk-ztkWxiQLkSs5duu1LzG_N57dVj2cAjAuxfGohJyaTCuBWO1sqT9CjDSWpSnh9Z_PyC3KvUbCW6Z6UR-MHWqb4-dGkvRC2B49ayTsYSU=-no)

```
$ sudo apt install flex bison texinfo xa65
```

2. Download

Download v3.4 or newer:
https://sourceforge.net/projects/vice-emu/files/releases/vice-3.4.tar.gz/download

3. Compile and install

```
$ tar xf vice-3.4.tar.gz
$ cd vice-3.4
$ ./configure --enable-sdlui2 --enable-x64
$ make
$ sudo make install
```

BUG: No sound, although sound support is outside the scope of this doc.

### DOSBox staging

![dosbox](https://lh3.googleusercontent.com/EgbS9fQlDsgfRQd5Mw3DvF8B9Q8LkEuvygFdY3jxamWlFKFah8IFerdK1-tBTEAmknswCxapP_KjCuPqYSryJ5Dc-jCd0NcvDYyfg7TISIDO81Jbaxp55F5xZRSqlKMgFbZdd_kFLJQ=-no)

1. Install dependencies

```
$ sudo apt install libopusfile-dev
```

2. Download source code, compile and install

```bash
$ git clone https://github.com/dosbox-staging/dosbox-staging.git
$ cd dosbox-staging
$ ./autogen.sh
$ ./configure
$ make
$ sudo make install
```

Note: DOSBox relies on OpenGL for hw-accel, but SDL was compiled with OpenGL ES2.
That means that DOSBox won't be hardware-accelerated.

## Feedback, Questions

Ask them here: https://forum.odroid.com/viewtopic.php?f=202&t=38789
