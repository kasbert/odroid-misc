#!/usr/bin/python3

# This script reads stdin and injects it as a keyboard event.
# Useful to simulate a keyboard in Odroid Advance Go.
# Use Case:
#  - You launch a C64 game using the C64 emulator
#  - The game asks you if you want [H]igh score or [T]raining mode.
#  - You cannot press any key since you don't have a keyboard.
#  - You ssh into the OGA
#  - run this script and voila, you can inject key events in the C64 emulator.

import time
from evdev import UInput, InputDevice, ecodes as e
import sys

class _GetchUnix:
    def __init__(self):
        import tty, sys

    def __call__(self):
        import sys, tty, termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch
getch = _GetchUnix()

kb = UInput(name='keyboard')
print(kb)

conv = {
        13: e.KEY_ENTER,

        27: e.KEY_ESC,
        32: e.KEY_SPACE,
        33: e.KEY_F1,   # shift + 1
        35: e.KEY_F3,   # shift + 3
        36: e.KEY_F4,   # shift + 4
        37: e.KEY_F5,   # shift + 5
        38: e.KEY_F7,   # shift + 7
        40: e.KEY_F9,   # shift + 9
        41: e.KEY_F10,  # shift + 0
        42: e.KEY_F8,   # shift + 8
        43: e.KEY_F12,  # shift + +

        48: e.KEY_0,
        49: e.KEY_1,
        50: e.KEY_2,
        51: e.KEY_3,
        52: e.KEY_4,
        53: e.KEY_5,
        54: e.KEY_6,
        55: e.KEY_7,
        56: e.KEY_8,
        57: e.KEY_9,

        64: e.KEY_F2,   # shift + 2

        73: e.KEY_UP,      # I
        74: e.KEY_LEFT,    # J
        75: e.KEY_DOWN,    # K
        76: e.KEY_RIGHT,   # L

        94: e.KEY_F6,   # shift + 6
        95: e.KEY_F11,  # shift + -

        65+32: e.KEY_A, # 97
        66+32: e.KEY_B,
        67+32: e.KEY_C,
        68+32: e.KEY_D,
        69+32: e.KEY_E,
        70+32: e.KEY_F,
        71+32: e.KEY_G,
        72+32: e.KEY_H,
        73+32: e.KEY_I,
        74+32: e.KEY_J,
        75+32: e.KEY_K,
        76+32: e.KEY_L,
        77+32: e.KEY_M,
        78+32: e.KEY_N,
        79+32: e.KEY_O,
        80+32: e.KEY_P,
        81+32: e.KEY_Q,
        82+32: e.KEY_R,
        83+32: e.KEY_S,
        84+32: e.KEY_T,
        85+32: e.KEY_U,
        86+32: e.KEY_V,
        87+32: e.KEY_W,
        88+32: e.KEY_X,
        89+32: e.KEY_Y,
        90+32: e.KEY_Z, # 122

        127: e.KEY_BACK,
        }

while True:
    c = getch()
    if c is 'Q':
        break
    code = ord(c)
    if code in conv.keys():
        cc = conv[code]
        kb.write(e.EV_KEY, cc, 1)
        kb.syn()
        time.sleep(0.2)
        kb.write(e.EV_KEY, cc, 0)
        kb.syn()
    else:
        print('Unknown code: ', code)

kb.close()
